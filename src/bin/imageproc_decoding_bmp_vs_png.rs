extern crate criterion;
extern crate image;

use criterion::{Bencher, Criterion};

use std::path::Path;

fn main() {
    // Paths has to be relative to crate root, from where criterion is run.
    fn decode_bmp_image(b: &mut Bencher) {
        b.iter_with_large_drop(|| {
            image::open(&Path::new("resources/pigeons.bmp")).unwrap()
        });
    }
    fn decode_png_image(b: &mut Bencher) {
        b.iter_with_large_drop(|| {
            image::open(&Path::new("resources/pigeons.png")).unwrap()
        });
    }
    Criterion::default().sample_size(20).noise_threshold(0.05)
        .bench_function("decode_bmp_image", decode_bmp_image)
        .bench_function("decode_png_image", decode_png_image);
}
